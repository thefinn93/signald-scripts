# signald scripts
*little scripts and utilities to use with signald*


## `trust-new-keys.sh`
I run this on a cron on most of my bots. It looks for any keys that are marked as untrusted and trusts them.
If the environment variable `REPORT_GROUP` is set, it sends a report to that group. `REPORT_GROUP` should be
the base64 encoded group ID.

## `signald-listener@.service`
A simple systemd unit file that receives new messages. This is used to keep bot users who
don't otherwise subscribe to the incoming message socket in sync with key updates, group
membership updates, etc. Install it by putting the file at `/etc/systemd/system/signald-listener@.service`,
and start it for a given account with `sudo systemctl start signald-listener@12024561414`,
with the account's phone number, without the + (because it makes systemd unhappy).
To start it automatically on system boot: `sudo systemctl enable signald-listener@12024561414`.
