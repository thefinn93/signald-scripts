#!/bin/bash
set -euo pipefail

signald() {
	echo "$1" | nc -q0 -U /var/run/signald/signald.sock | jq 'select(.type != "version")'
}

signald '{"type": "list_accounts"}' | jq -r 'select(.type == "account_list") | .data.accounts[].username' | while read username; do
	signald "{\"type\": \"get_identities\", \"username\": \"$username\"}" | jq -rc ".data.identities[] | select(.trust_level == \"UNTRUSTED\") | \"{\\\"type\\\": \\\"trust\\\", \\\"fingerprint\\\": \\\"\(.fingerprint)\\\", \\\"recipientNumber\\\": \\\"\(.username)\\\", \\\"username\\\": \\\"$username\\\"}\"" | while read trustcmd; do
		signald "$trustcmd"
		if [ ! -z ${REPORT_GROUP+} ]; then
			signald "{\"type\": \"send\", \"recipientGroupId\": \"${REPORT_GROUP}\", \"username\": \"$username\", \"messageBody\": \"$(echo $trustcmd | jq -r .recipientNumber)'s new fingerprint is $(echo $trustcmd | jq -r .fingerprint)\"}"
		fi
	done
done
